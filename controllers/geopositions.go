// Bundle of functions managing the CRUD and the elasticsearch engine
package controllers

import (
	"github.com/jinzhu/gorm"
	"bitbucket.org/quorumteam/geoposition/models"
	"github.com/quorumsco/logs"
)

// Geoposition contains the geoposition related methods and a gorm client
type Geoposition struct {
	DB *gorm.DB
}

// RetrieveCollection calls the GeopositionSQL Find method and returns the results via RPC
func (t *Geoposition) RetrieveCollection(args models.GeopositionArgs, reply *models.GeopositionReply) error {
	var (
		geopositionStore = models.GeopositionStore(t.DB)
		err       error
	)

	if reply.Geopositions, err = geopositionStore.Find(args); err != nil {
		logs.Error(err)
		return err
	}

	return nil
}

// // RetrieveCollection by Type calls the GeopositionSQL Find method and returns the results via RPC
// func (t *Geoposition) RetrieveCollectionByType(args models.GeopositionArgs, reply *models.GeopositionReply) error {
// 	var (
// 		geopositionStore = models.GeopositionStore(t.DB)
// 		err       error
// 	)
//
// 	if reply.Geopositions, err = geopositionStore.FindByType(args); err != nil {
// 		logs.Error(err)
// 		return err
// 	}
//
// 	return nil
// }
//
// // RetrieveCollection by Name calls the GeopositionSQL Find method and returns the results via RPC
// func (t *Geoposition) RetrieveCollectionByName(args models.GeopositionArgs, reply *models.GeopositionReply) error {
// 	var (
// 		geopositionStore = models.GeopositionStore(t.DB)
// 		err       error
// 	)
//
// 	if reply.Geopositions, err = geopositionStore.FindByName(args); err != nil {
// 		logs.Error(err)
// 		return err
// 	}
//
// 	return nil
// }
//
// // RetrieveCollection by Type and Name calls the GeopositionSQL Find method and returns the results via RPC
// func (t *Geoposition) RetrieveCollectionByTypeAndName(args models.GeopositionArgs, reply *models.GeopositionReply) error {
// 	var (
// 		geopositionStore = models.GeopositionStore(t.DB)
// 		err       error
// 	)
//
// 	if reply.Geopositions, err = geopositionStore.FindByTypeAndName(args); err != nil {
// 		logs.Error(err)
// 		return err
// 	}
//
// 	return nil
// }

// Retrieve calls the GeopositionSQL First method and returns the results via RPC
func (t *Geoposition) Retrieve(args models.GeopositionArgs, reply *models.GeopositionReply) error {
	var (
		geopositionStore = models.GeopositionStore(t.DB)
		err       error
	)

	if reply.Geoposition, err = geopositionStore.First(args); err != nil {
		logs.Error(err)
		return err
	}

	return nil
}

// Update calls the GeopositionSQL Update method and returns the results via RPC
func (t *Geoposition) Update(args models.GeopositionArgs, reply *models.GeopositionReply) error {
	var (
		geopositionStore = models.GeopositionStore(t.DB)
		err       error
	)

	if err = geopositionStore.Save(args.Geoposition, args); err != nil {
		logs.Error(err)
		return err
	}

	if reply.Geoposition, err = geopositionStore.First(args); err != nil {
		return err
	}

	return nil
}

// Create calls the GeopositionSQL Create method and returns the results via RPC
func (t *Geoposition) Create(args models.GeopositionArgs, reply *models.GeopositionReply) error {
	var (
		geopositionStore = models.GeopositionStore(t.DB)
		err       error
	)

	if err = geopositionStore.Save(args.Geoposition, args); err != nil {
		logs.Error(err)
		return err
	}

	reply.Geoposition = args.Geoposition

	return nil
}

// Delete calls the GeopositionSQL Delete method and returns the results via RPC
func (t *Geoposition) Delete(args models.GeopositionArgs, reply *models.GeopositionReply) error {
	var (
		geopositionStore = models.GeopositionStore(t.DB)
		err       error
	)

	if err = geopositionStore.Delete(args.Geoposition, args); err != nil {
		logs.Debug(err)
		return err
	}

	return nil
}
