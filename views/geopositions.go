// Views for JSON responses
package views

import "bitbucket.org/quorumteam/geoposition/models"

// Forms is a type used for JSON request responses
type Geopositions struct {
	Geopositions []models.Geoposition `json:"geopositions"`
}

// Form is a type used for JSON request responses
type Geoposition struct {
	Geoposition *models.Geoposition `json:"geoposition"`
}
