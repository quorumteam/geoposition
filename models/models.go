// Definition of the structures and SQL interaction functions
package models

import (
	_ "github.com/lib/pq"
)

// Models return one of every model the database must create..
func Models() []interface{} {
	return []interface{}{
		&Geoposition{},
	}
}
