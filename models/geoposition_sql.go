// Definition of the structures and SQL interaction functions
package models

import (
	"errors"

	"github.com/jinzhu/gorm"
)

// GeopositionSQL contains a Gorm client and the geoposition and gorm related methods
type GeopositionSQL struct {
	DB *gorm.DB
}

// Save inserts a new geoposition into the database
func (s *GeopositionSQL) Save(c *Geoposition, args GeopositionArgs) error {
	if c == nil {
		return errors.New("save: geoposition is nil")
	}

	c.GroupID = args.Geoposition.GroupID
	if c.ID == 0 {
		err := s.DB.Create(c).Error
		s.DB.Last(c)
		return err
	}

	return s.DB.Where("group_id = ?", args.Geoposition.GroupID).Save(c).Error
}

// Delete removes a geoposition from the database
func (s *GeopositionSQL) Delete(c *Geoposition, args GeopositionArgs) error {
	if c == nil {
		return errors.New("delete: geoposition is nil")
	}

	return s.DB.Where("group_id = ?", args.Geoposition.GroupID).Delete(c).Error
}

// First returns a geoposition from the database using his ID
func (s *GeopositionSQL) First(args GeopositionArgs) (*Geoposition, error) {
	var f Geoposition
	//var r Refvalue

	if err := s.DB.Where(args.Geoposition).First(&f).Error; err != nil {
		if s.DB.Where(args.Geoposition).First(&f).RecordNotFound() {
			return nil, nil
		}
		return nil, err
	}

	return &f, nil
}


// Find returns all the geopositions with a given groupID from the database
func (s *GeopositionSQL) Find(args GeopositionArgs) ([]Geoposition, error) {
	var geopositions []Geoposition

	err := s.DB.Where("group_id = ?", args.Geoposition.GroupID).Find(&geopositions).Error
	if err != nil {
		return nil, err
	}

	return geopositions, nil
}
//
// // Find returns all the geopositions with a given groupID from the database
// func (s *GeopositionSQL) FindByType(args GeopositionArgs) ([]Geoposition, error) {
// 	var geopositions []Geoposition
//
// 	err := s.DB.Where("group_id = ?", args.Geoposition.GroupID).Where("type = ?", args.Geoposition.Type).Find(&geopositions).Error
// 	if err != nil {
// 		return nil, err
// 	}
//
// 	return geopositions, nil
// }
//
// // Find returns all the geopositions with a given groupID and a given name from the database
// func (s *GeopositionSQL) FindByName(args GeopositionArgs) ([]Geoposition, error) {
// 	var geopositions []Geoposition
// 	//var refvalues []Refvalue
//
// 	//db.Preload("Town").Find(&places)
// 	//err := s.DB.Where("group_id = ?", args.Geoposition.GroupID).Where("name = ?", args.Geoposition.Name).Find(&geopositions).Error
// 	err := s.DB.Where("group_id = ?", args.Geoposition.GroupID).Where("name = ?", args.Geoposition.Name).Preload("Refvalues").Find(&geopositions).Error
// 	if err != nil {
// 		return nil, err
// 	}
// 	// for i, _ := range geopositions {
//   //   s.DB.Model(geopositions[i]).Related(&geopositions[i].Refvalues)
// 	// }
// 	//s.DB.Model(&geopositions).Related(&refvalues)
// 	return geopositions, nil
// }
//
// // Find returns all the geopositions with a given groupID from the database
// func (s *GeopositionSQL) FindByTypeAndName(args GeopositionArgs) ([]Geoposition, error) {
// 	var geopositions []Geoposition
//
// 	err := s.DB.Where("group_id = ?", args.Geoposition.GroupID).Where("type = ?", args.Geoposition.Type).Where("name = ?", args.Geoposition.Name).Find(&geopositions).Error
// 	if err != nil {
// 		return nil, err
// 	}
//
// 	return geopositions, nil
// }
