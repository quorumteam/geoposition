// Definition of the structures and SQL interaction functions
package models

import "github.com/jinzhu/gorm"

// GeopositionDS implements the GeopositionSQL methods
type GeopositionDS interface {
	Save(*Geoposition, GeopositionArgs) error
	Delete(*Geoposition, GeopositionArgs) error
	First(GeopositionArgs) (*Geoposition, error)
	Find(GeopositionArgs) ([]Geoposition, error)
	// FindByType(GeopositionArgs) ([]Geoposition, error)
	// FindByName(GeopositionArgs) ([]Geoposition, error)
	// FindByTypeAndName(GeopositionArgs) ([]Geoposition, error)

	// FindNotes(*Geoposition, *GeopositionArgs) error
	// FindTags(*Geoposition) error
}

// Geopositionstore returns a GeopositionDS implementing CRUD methods for the forms and containing a gorm client
func GeopositionStore(db *gorm.DB) GeopositionDS {
	return &GeopositionSQL{DB: db}
}
