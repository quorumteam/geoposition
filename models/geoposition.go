// Definition of the structures and SQL interaction functions
package models

import (
	"time"
)

// Geoposition represents all the components of a form
type Geoposition struct {
	ID       uint   `gorm:"primary_key" db:"id" json:"id"`
	UserID     uint `db:"userid" json:"userid"`
	Date    *time.Time `json:"date,omitempty"`
	Latitude  string `json:"latitude,omitempty"`
	Longitude string `json:"longitude,omitempty"`
	Location  string `json:"location,omitempty"` // as "lat,lon" (for elasticsearch)

	GroupID uint `sql:"not null" db:"group_id" json:"group_id"`
}

// GeopositionArgs is used in the RPC communications between the gateway and Geopositions
type GeopositionArgs struct {
	Geoposition *Geoposition
}

// GeopositionReply is used in the RPC communications between the gateway and Geopositions
type GeopositionReply struct {
	Geoposition  *Geoposition
	Geopositions []Geoposition
}

// Validate checks if the contact is valid
func (c *Geoposition) Validate() map[string]string {
	var errs = make(map[string]string)

	// if c.Firstname == "" {
	// 	errs["firstname"] = "is required"
	// }

	// if c.Surname == "" {
	// 	errs["surname"] = "is required"
	// }

	// if c.Mail != nil && !govalidator.IsGeoposition(*c.Mail) {
	// 	errs["mail"] = "is not valid"
	// }

	return errs
}
